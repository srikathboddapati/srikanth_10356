import React, { Component}  from 'react';
import Clock from './components/Clock.jsx';
import  Registration from './components/Registration.jsx';
import  FetchUsers from './components/FetchUsers.jsx';
import  Car from './components/Car.jsx';
import ReactDOM from 'react-dom';
import { BrowserRouter , Route, Link } from 'react-router-dom';
import  displayproducts from './components/displayproducts.jsx';
import  Cardetails from './components/Cardetails.jsx';
import  addproduct from './components/addproduct.jsx';
import  Home from './components/home.jsx';
import  counter from './components/counter.js';
import  news from './components/news.js';
import  NewsDetails from './components/news_details.js';
import  Login from './components/login.js';
import  user from './components/user.js';
import  admindashboard from './components/admindashboard.js';
import practice from './components/practice';
import Practice2 from './components/practice2';
const Routes = () => (
  <BrowserRouter>
    <div>
        <Route exact path={'/'} component={Home} exact/> 
        <Route path={'/login'} component={Login}/>
        <Route path={'/clock'} component={Clock}/>
        <Route path={'/admindashboard'} component={admindashboard} exact/>
        <Route path={'/registration'} component={Registration}/>
        <Route path={'/FetchUsers'} component={FetchUsers}/>
        <Route path={'/Car'} component={Car}/>
        <Route path={'/displayproducts'} component={displayproducts}/>
        <Route path={'/addproduct'} component={addproduct}/>
        <Route path={'/Cars/:id'} component={Cardetails}/>
        <Route path={'/news'} component={news} exact/>
        <Route path={'/user'} component={user}/>
        <Route path={'/news/:id'} component={NewsDetails}/>
        <Route path={'/practice'} component={practice}/>
    </div>
  </BrowserRouter>
);
export default Routes;