import React, { Component } from 'react';
class FetchUsers extends Component {
    constructor(props) {
        super(props);
        this.state = { items: [], isLoaded: false };
    }

    componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/users').then(res => res.json()).then(data => {
            this.setState({ items: data, isLoaded: true })
        });
    }


    render() {
        if (!this.state.isLoaded) {
            return <div>is loading</div>

        }
        return (

            <ul>{
                this.state.items.map(item => (
                    <li key="{item.id}">name :{item.name}  | email :{item.email} </li>
                )
                )
            }

            </ul>
        );
    }
}
export default FetchUsers;