import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter , Route, Link } from 'react-router-dom';

class Car extends Component{
   constructor(props){
       super(props);
       this.state = {cars: []};
   }
   componentDidMount(){
       const data = [
           {
               id : 1,
               name : 'honda',
               year : '2010',
               model: 'Accord Crosstour',
               make : 'Honda'
           },
           {
               id : 2,
               name : 'benze',
               year : '2016',
               model:'Amg',
               make:'Mercedes Benz'
           },
           {
               id:3,
               name :  'BMW X6 SUV',
               year : '2015',
               model:'bmw120',
               make : 'BMW'
           },
           {
               id : 4,
               name:'Ford',
               year:'2012',
               model:'ford123',
               make:'Edge'
           }
       ];
   
   this.setState({cars : data});
    }
   
   render() {
       const carnode = this.state.cars.map(car => {
           return (
           <Link to={"cars/"+ car.id}  key ={car.id}>
           {car.name}
           </Link>
           )
       });
       return(
         <div>
             <div className = "list-group">
                {carnode}
               
             </div>
         </div>
        
       );
   }
}
export default Car;