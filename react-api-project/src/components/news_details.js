import React, { Component } from 'react';
import * as newsActions from '../store/actions/newsActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class NewsDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            newsItem: {}
        }
    }
    componentWillMount() {
        this.props.newsActions.fetchNewsByID(this.props.match.params.id);
    

    }
    componentWillReceiveProps(newProps) {
        if (newProps.newsItem) {
            this.setState({
                newsItem: newProps.newsItem,
            })
           
        }
    }
    render() {
  


        return (
            <React.Fragment>
                <div>
                  body:{this.state.newsItem.body}
                </div>
                <div>
                    publishDate : {this.state.newsItem.timestamp}
                </div>



            </React.Fragment>

        );
    }
}

function mapStateToProps(state) {
      

    return {
        newsItem: state.newsReducer.newsItem,
        
    };
    
}

function mapDispatchProps(dispatch) {
   
    return {
        newsActions: bindActionCreators(newsActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchProps)(NewsDetails);