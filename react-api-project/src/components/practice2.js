import React,{Component} from 'react';

 const Practice2 = (props) => {
    let age = props.age?props.age:"no age enter";
    return(

        <p>Name:{props.children} | Age: {age}</p>
    )
}
export default Practice2;
