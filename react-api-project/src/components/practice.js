import React, { Component } from 'react';

import Practice2 from './practice2';
class practice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            age: ''
        }
        this.changeUserAge = this.changeUserAge.bind(this);
    }

    changeUserAge = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }
    render() {
        return (
            <div>
                <div>
                    <h1>user List</h1>

                    <Practice2 age={this.state.age}>jophn</Practice2>
                    <Practice2 age={this.state.age}>jill</Practice2>
                    <Practice2 age={this.state.age}>peeter</Practice2>
                    <Practice2 age={this.state.age}>srikanth</Practice2>
                </div>
                <div>
                    <form>
                        age:
                <input type="text" name="age" value={this.state.age} onChange={this.changeUserAge} />

                    </form>
                </div>
            </div>
        );
    }


}


export default practice;