import React, { Component } from 'react';
import * as newsActions from '../store/actions/newsActions';
import * as authActions from '../store/actions/authActions';
import { Redirect} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import  Login from './login.js';

import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
class user extends Component {

    constructor(props){
        super(props);
        this.state = {
            islogout : false,
            user_data : {}

        }
        this.logout = this.logout.bind(this);
    }

    logout(e){
        e.preventDefault();
        localStorage.removeItem('jwt-token');
        this.setState({
            islogout : true
        })

    }

   
    
    render() {

        if(this.state.islogout){
            return <Redirect to ='/login' />

        }
      

      
        return (
            <div className="user">
              <h2>role is :{this.props.user_data.role}</h2>
              <ul>
                <li><a href = "#" onClick = {this.logout}>logout</a></li>
            </ul>
            </div>
        );
    }
}
function mapStateToProps(state) {
    console.log("call mapstateprops in login.js");
      
      return {
        user_data: state.authReducer.user_data
      };
      console.log("call mapstateprops2 in login.js");
  }
  
  function mapDispatchProps(dispatch) {
      console.log("call mapdispatchpropsfile in login.js");
      return {
       authActions: bindActionCreators(authActions, dispatch),
      };
      console.log("call mapdispatchpropsfile2 in login.js");
  }

  export default connect(mapStateToProps, mapDispatchProps)(user);
  


