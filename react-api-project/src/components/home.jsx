import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
class Home extends Component {
    render() {
        return (
            <div className="something">
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/login">Login</Link>
                    </li>

                    <li>
                        <Link to="/clock">clock</Link>
                    </li>
                    <li>
                        <Link to="/registration">registration</Link>
                    </li>
                    <li>
                        <Link to="/fetchUsers">fetchUsers</Link>
                    </li>
                    <li>
                        <Link to="/Car">Car</Link>
                    </li>
                    <li>
                        <Link to="/displayproducts">displayproducts</Link>
                    </li>
                    <li>
                        <Link to="/addproduct">addproduct</Link>
                    </li>
                    <li>
                        <Link to="/Counter">Counter</Link>
                    </li>
                    <li>
                        <Link to="/news">News</Link>
                    </li>
                    <li>
                        <Link to="/admindashboard">admindashboard</Link>
                    </li>
                    <li>
                        <Link to="/practice">practice</Link>
                    </li>
                </ul>

            </div>  
        );
    }
}
export default Home;