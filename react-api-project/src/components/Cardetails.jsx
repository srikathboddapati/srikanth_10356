import React, { Component } from 'react';
class Cardetails extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const Cars = [
            {
                id: 1,
                name: 'honda',
                year: '2010',
                model: 'Accord Crosstour',
                make: 'Honda'
            },
            {
                id: 2,
                name: 'benze',
                year: '2016',
                model: 'Amg',
                make: 'Mercedes Benz'
            },
            {
                id: 3,
                name: 'BMW X6 SUV',
                year: '2015',
                model: 'bmw120',
                make: 'BMW'
            },
            {
                id: 4,
                name: 'Ford',
                year: '2012',
                model: 'ford123',
                make: 'Edge'
            }
        ];
        const id = this.props.match.params.id;
        const car = Cars.filter(car => {
            if (car.id == id) {
                return car;
            }
        }

        );

        return (
            <div>
                <h1>{car[0].name}</h1>
                <div className='thumbnails'>
                    <ul>
                        <li><strong>id:</strong>{car[0].id}</li>
                        <li><strong>name:</strong>{car[0].name}</li>
                        <li><strong>year:</strong>{car[0].year}</li>
                        <li><strong>Model:</strong>{car[0].model}</li>
                        <li><strong>make:</strong>{car[0].make}</li>
                    </ul>

                </div>

            </div>

        );
    }
}
export default Cardetails;