import React, { Component } from 'react';
import * as newsActions from '../store/actions/newsActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import './news.css';

class News extends Component {


    componentDidMount() {
        this.props.newsActions.fetchNews();



    }

    render() {


        var newsblock = () => {

            return <div>
                {
                    (this.props.newsItems) ? <div>
                        {this.props.newsItems.map(item => (
                            <div className="carddata">
                                <div className="image-wrapper">
                                    <img src="https://aidforcancer.com/static/media/newsDefault.33e07ba7.jpg" alt="logo" />
                                </div>
                                <div className="content">
                                    <h6 className="heading-txt" key={item._id}>
                                        <a href={`/news/${item._id}`}>{item.heading}</a></h6>
                                    <p className="post-link-text">Drs. Sudha Sivaram, Center for Global Health, and Vikrant Sahasrabuddhe, Division of Cancer Prevention, work closely to advance cancer research at the National Cancer Institute.</p>
                                </div>
                                <div className="likescomments">
                                    <div className="info-detail-bar flex-container-row">
                                        <div className="like-count-wrapper">
                                            <i className="fa fa-thumbs-up" aria-hidden="true"></i>
                                            <span className="like-count-digit">70</span>
                                        </div>
                                        <div className="comments-count-wrapper">
                                            <i className="fa fa-comments" aria-hidden="true"></i>
                                            <span className="comments-count-digit">48</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        ))} </div> : <div>Loading....</div>
                }
            </div>

        }



        return (
            <div className="">
                <nav className="navbar navbar-expand-lg navbar-light">
                    <a className="navbar-brand" href="#">Navbar</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item active">
                                <a className="nav-link" href="#">Information <span className="sr-only">(current)</span></a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Opinions</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Forum</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Volunter</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Seek Help</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Sign In</a>
                            </li>
                        </ul>

                    </div>
                </nav>

                <div className="container">
                    <div className="row">
                        <div className="hero-image">
                            <div className="hero-text">
                                <h3>Information about Cancer</h3>
                                <p className="page-desc-text bold-text">Whether you or someone you love has cancer, knowing what to expect can help you cope. From basic information about cancer and its causes, to in-depth information on specific cancer types- including risk factors, early detection, diagnosis and treatment options- you will find it here.</p>
                            </div>

                        </div>

                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-3">
                            <div className="sidenavbar">
                                <a href="#">News</a>
                                <a href="#">NGOs/NPOs</a>
                                <a href="#">Support group</a>
                                <a href="#">Online help</a>
                            </div>
                        </div>
                        <div className="col-md-9">
                            <div className="newsheading">
                                <div className="news">
                                    <span>NEWS</span>
                                </div>
                                <div className="showall">
                                    <span>Show all</span>
                                </div>
                            </div>
                            <hr></hr>
                            <div className="row">
                                {newsblock()}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {


    return {
        newsItems: state.newsReducer.news,

    };
}

function mapDispatchProps(dispatch) {

    return {
        newsActions: bindActionCreators(newsActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchProps)(News);