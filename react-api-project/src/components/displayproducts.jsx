import React, { Component } from 'react';
import './table.css'

class displayproducts extends Component {
    constructor(props) {
        super(props);
        this.state = { items: [], isLoaded: false };
    }

    componentDidMount() {
        fetch('http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/displayProducts').then(res => res.json()).then(data => {
            this.setState({ items: data, isLoaded: true })
        });
    }
    render() {
        if (!this.state.isLoaded) {
            return <div>is loading</div>

        }
        return (
            <div>
                <h1>displaying all the products</h1>
                <table  >
                    <tr><th>productid</th><th>productname</th><th>price</th><th>quantity</th><th>productType</th></tr>


                    {
                        this.state.items.map(item => (
                            <tr key={item.product_id}>
                                <td>{item.product_id}</td>
                                <td>{item.productname}</td>
                                <td>{item.price}</td>
                                <td>{item.quantity}</td>
                                <td>{item.productType}</td>
                            </tr>
                        )
                        )
                    }


                </table>
            </div>
        );
    }
}
export default displayproducts;