import React, { Component } from 'react';
import * as authActions from '../store/actions/authActions';
import { Redirect} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
class admindashboard extends Component {

    constructor(props){
        super(props);
      this.state = {
          user_data : {}
      };
    }

    componentDidMount() {
        this.props.authActions.FETCH_USER();
    }

    render() {
        if(this.props.user_data){
          
            if(localStorage.getItem('jwt-token') && this.props.user_data.role == 'user'){
                return <Redirect to ='/user' />
            }
        }
      
        return (
            <div>
            {
                (this.props.user_data)?
                <div>
                <div>loading...!</div>
                </div>:
                <div>dashboard</div>
            }
            </div>
           
        );
    }
}


function mapStateToProps(state) {
    console.log("call mapstateprops in login.js");
      
      return {
        user_data: state.authReducer.user_data
      };
      console.log("call mapstateprops2 in login.js");
  }
  
  function mapDispatchProps(dispatch) {
      console.log("call mapdispatchpropsfile in login.js");
      return {
       authActions: bindActionCreators(authActions, dispatch),
      };
      console.log("call mapdispatchpropsfile2 in login.js");
  }

  export default connect(mapStateToProps, mapDispatchProps)(admindashboard);
