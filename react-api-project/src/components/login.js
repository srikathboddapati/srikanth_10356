import React, { Component } from 'react';
import * as newsActions from '../store/actions/newsActions';
import * as authActions from '../store/actions/authActions';
import { Redirect} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

class LogIn extends Component {

    constructor(props) {
        console.log("calling the constructe");
        super(props);
        this.state = {
            email : '',
            password : '',
            isSubmitted :false
        }
        this.handleChange = this.handleChange.bind(this);
        this.doLogin = this.doLogin.bind(this);
    };

    

    handleChange = e =>{
        console.log("handlechange method in login.js");
          this.setState({
              [e.target.name] : e.target.value
          });
         
          e.preventDefault();
      }

    doLogin = e => {
        console.log(this.state.isSubmitted + "dologin methos in login.js");
         this.props.authActions.doLoginUser({
             email : this.state.email,
             password : this.state.password
         })
         this.setState({ isSubmitted :true})

         console.log(this.state.isSubmitted + "dologin methos in login.js");
    }
  
    render() {
         
        if(this.state.isSubmitted){
            console.log("calling the redirection in login.js");
            if(localStorage.getItem('jwt-token') && this.props.isLoggedIn == true){
                return <Redirect to ='/admindashboard' />
            }
             else {
                 if(this.props.invalid_data.statuscode == -1)
                 alert(this.props.invalid_data.status);
             
             if(this.props.invalid_data.statuscode == -2)
             alert(this.props.invalid_data.status);
         
         if(this.props.invalid_data.statuscode == -3)
         alert(this.props.invalid_data.status);
     }
        
        }
        
        return (
           
        <div>
           
                <label>
                    Email:
                    <input type="text" name="email" value = { this.state.email} onChange={this.handleChange} />
                </label>
                <label>
                    password:
                    <input type="text" name="password" value = { this.state.password} onChange={this.handleChange} />
                </label>
                <input type = "submit" onClick = {this.doLogin} value="submit"></input>
           
        </div>

        );
    }
}

function mapStateToProps(state) {
    console.log("call mapstateprops in login.js");
      
      return {
          isLoggedIn: state.authReducer.is_loggged_in,
          invalid_data : state.authReducer.invalid_data
      };
      console.log("call mapstateprops2 in login.js");
  }
  
  function mapDispatchProps(dispatch) {
      console.log("call mapdispatchpropsfile in login.js");
      return {
       authActions: bindActionCreators(authActions, dispatch),
      };
      console.log("call mapdispatchpropsfile2 in login.js");
  }

  export default connect(mapStateToProps, mapDispatchProps)(LogIn);

