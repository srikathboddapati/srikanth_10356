import React, { Component } from 'react';
import ReactDOM from 'react-dom';
class addproduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            productname : '',
            productprice : '',
            productquantity :'',
            producttype : ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.doPost = this.doPost.bind(this);

    }

    handleChange = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        });
       
    }
    doPost = (e) => {

        var data = {
            productname: this.state.productname,
            productprice: this.state.productprice,
            productquantity: this.state.productquantity,
            producttype: this.state.producttype
         }
         debugger;
        fetch('https://http://192.168.150.65:8080/RetailBillingWebServer/rest/stocker/addProducts', {
            headers: { "Content-Type": "application/json; charset=utf-8" },
            method: 'POST',
            body: JSON.stringify({
                data
            })
        });

    }

    render() {
        return (
            <div className="">
                <form>

                    <label>
                        productname:
                    <input type="text" name="productname" value={this.state.productname} onChange={this.handleChange} />
                    </label>
                    <label>
                        productprice:
                    <input type="text" name="productprice" value={this.state.productprice} onChange={this.handleChange} />
                    </label>

                    <label>
                        productquantity:
                     <input type="text" name="productquantity" value={this.state.productquantity} onChange={this.handleChange} />
                    </label>
                    <label>
                        producttype
                    <input type="text" name="producttype" value={this.state.producttype} onChange={this.handleChange} />
                    </label>
                    <input type="submit" onClick={this.doPost} value="submit"></input>
                </form>
            </div>
        );
    }
}
export default addproduct;