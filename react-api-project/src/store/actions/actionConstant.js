export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
/**
 * action constants for news scenarios
 */
 export const FETCH_NEWS = 'FETCH_NEWS';
 export const RECEIVE_NEWS = 'RECIEVE_NEWS';


  export const FETCH_NEWS_BY_ID = 'FETCH_NEWS_BY_ID';
  export const RECEIVE_NEWS_BY_ID = 'RECIEVE_NEWS_BY_ID';
 

  /**
   * actions constants for login scenarions
   */
  export const DO_LOGIN_USER = 'DO_LOGIN_USER';
  export const LOGIN_USER_SUCESS = 'LOGIN_USER_SUCESS';
  export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

  export const FETCH_USER = 'FETCH_USER';
  export const RECEIVE_USER = 'RECEIVE_USER';

 export const LOGOUT = 'USERS_LOGOUT';
  console.log("all action constants for login.js ");