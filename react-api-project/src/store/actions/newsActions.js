import * as allActions from './actionConstant';

export function receiveNews(data){

    return {
        type: allActions.RECEIVE_NEWS, payload : data
        
    };
   
}

export function fetchNews(){
  
    return {
        type: allActions.FETCH_NEWS, payload :{} 
    };
    
}

export function receiveNewsByID(data){

    return {
        type: allActions.RECEIVE_NEWS_BY_ID, payload : data
        
    };
   
}


export function fetchNewsByID(id){
  
    return {
        type: allActions.FETCH_NEWS_BY_ID, payload :{id} 
    };
    
}