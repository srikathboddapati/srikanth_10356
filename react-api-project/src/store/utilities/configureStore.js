import { createStore, applyMiddleware,compose  } from 'redux';
import rootReducer from '../reducer/rootReducer';
import newsService from '../middleware/newsService';
import AuthService from '../middleware/authService';

export default function configureStore(){
    return createStore(rootReducer,
        compose(applyMiddleware(newsService,AuthService))
        
    );
    console.log("calling configureStore ");
    
}