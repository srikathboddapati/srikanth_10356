import request from 'superagent';
import * as allActions from '../actions/actionConstant';
import * as authActions from '../actions/authActions';

const authService = store => next => action => {

    next(action)
    switch(action.type){
        case allActions.DO_LOGIN_USER:
        
      request.post('http://13.250.235.137:8050/api/auth/login',action.payload)
        .then(res =>{
            console.log("in middleware calling the post method when sucess")
            const data = JSON.parse(res.text);
            if(data.statuscode == 1){
                next({
                    type: allActions.LOGIN_USER_SUCESS,
                    payload:data.statuscode
                })
                
                
                localStorage.setItem('jwt-token',res.body.token);

                console.log("getting token into localstorage");
            }
           
            else {
                next(
                    {
                        type:allActions.LOGIN_USER_FAILURE,
                        payload:data
                    }
                );

            }
        })
        .catch(err =>{
            return next({
                type:"LOGIN_USER_DATA_ERROR",err
            });
        });

        break;
         
        case allActions.FETCH_USER:
          
      request.get('http://13.250.235.137:8050/api/user')
      .send(action.payload)
      .set('Accept', 'application/json')
      .set('Authorization', ('Bearer '+ localStorage.getItem('jwt-token')))
      .then(res =>{
      
          const data = JSON.parse(res.text);
          if(data.statuscode == 1){
            next(authActions.RECEIVE_USER(data));
              
              
          } else {
              next(
                  {
                      type:"USER_DATA_ERROR",
                      payload:data.statuscode
                  }
              );

          }
      })
      .catch(err =>{
          return next({
              type:"USER_DATA_ERROR",err
          });
      });

      break;


        default :
        console.log("default authservice middleware");
        break;
    }
}

export default authService;