import request from 'superagent';
import * as allActions from '../actions/actionConstant'
import * as newsAction from '../actions/newsActions'

const newsService = (store) => next => action => {
    next(action)
   
    switch (action.type) {
        case allActions.FETCH_NEWS:
        console.log("news.js service");
            request.get('https://www.aidforcancer.com:8000/api/news/recent').then(res => {
                const data = JSON.parse(res.text);
              
                next(newsAction.receiveNews(data));
                
            })
                .catch(err => {
                    next({
                        type: 'FETCH_NEWS_DATA_ERROR', err
                    });
                });
            break;

            case allActions.FETCH_NEWS_BY_ID:
            console.log("newsdetails.js service");
         
            request.get(`https://www.aidforcancer.com:8000/api/news`+`/${action.payload.id}`)
            .then(res => {
                const data = JSON.parse(res.text);
                next(newsAction.receiveNewsByID(data));

            });
            
            break;

        default:
            break;
    };
};

export default newsService;