import * as allActions from '../actions/actionConstant';

const initialState = {
    news : [],
    isLoaded : false,
    newsItem : {}
}

export default function newsReducer(state = initialState,action){
    switch(action.type){
        case allActions.FETCH_NEWS:
   
        return action;
        case allActions.RECEIVE_NEWS:

        return {
            ...state,
            news : action.payload.articles,
            isLoad : true

        };
        case allActions.FETCH_NEWS_BY_ID:
 
        return action;

        case allActions.RECEIVE_NEWS_BY_ID:
    
        return {
        
            ...state,
            newsItem : action.payload.article
         
             
        };

        default: 
      
        return state;
    }

}