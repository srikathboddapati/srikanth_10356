package com.hrmanagement.unittest;
import static org.junit.Assert.*;

import java.sql.Timestamp;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.hrmanagement.bean.Employee;
import com.hrmanagement.services.EmployeeService;
import com.hrmanagement.services.EmployeeServiceImpl;

public class EmployeeServiceUnitTest {

	EmployeeService employeeService = null;
	EmployeeServiceImpl employeeServiceImpl = null;
	Employee employee = null;
	
	@Before
	public void setUp() {
		employeeServiceImpl = new EmployeeServiceImpl();
		employee = new Employee();
	}
	

	@After
	public void setDown() {
		employeeService = null;
		employeeServiceImpl = null;
		employee = null;
	}
	/**
	 * test for employee data insert  into db 
	 */
	
	@Ignore
	@Test
	public void UnitTestInsertNewEmployee() {
		employee.setE_id(1);
		employee.setE_name("srikanth");
		employee.setE_password("12a31a0311");
		employee.setE_mail("srikanth@gmail.com");
		employee.setPhno("9874561230");
		employee.setSalary(10000);
		employee.setDOB("08/12/1994");
		Timestamp date = new Timestamp(new java.util.Date().getTime());
		employee.setDOJ(date);
		employee.setDomain_id(1);
		assertTrue(employeeServiceImpl.insertEmployeeSerice(employee));
		
	}
	
	/**
	 * negative case for inserting new employee with same id because id is primary key
	 */
	@Ignore
	@Test
	public void UnitTestInsertSameNewEmployee(){
		//where this id all ready present in db
		employee.setE_id(6);
		employee.setE_name("srikanth");
		employee.setE_password("");
		employee.setE_mail("srikanth@gmail.com");
		employee.setPhno("9874561230");
		employee.setSalary(10000);
		employee.setDOB("08/12/1994");
		Timestamp date = new Timestamp(new java.util.Date().getTime());
		employee.setDOJ(date);
		employee.setDomain_id(1);
		assertFalse(employeeServiceImpl.insertEmployeeSerice(employee));
		
	}
	/**
	 * test case for employee id present
	 */
	@Ignore
	@Test
	public void UnitTestfindByIdService(){
		//here id is present
		int id = 1;
		assertNotNull(employeeServiceImpl.findByIdService(id));
	}
	
	/**
	 * test case for employee id not present
	 */
	@Ignore
	@Test
	public void NegativeUnitTestfindByIdService(){
		//where this id is not present in db
		int id = 10;
		assertNull(employeeServiceImpl.findByIdService(id));
	}
	
	/**
	 * test case for employee list is present
	 */
	@Ignore
	@Test
	public void UnitTestfetchAllEmployeeService(){
		assertNotNull(employeeServiceImpl.fetchAllEmployeeService());
	}
	/**
	 * test case for employee list not present
	 */
	@Ignore
	@Test
	public void NegativeUnitTestfetchAllEmployeeService(){
		//if the list not present but accessing th list
		assertNull(employeeServiceImpl.fetchAllEmployeeService());
	}
	
	/**
	 * test case for employee delete if the id is exist
	 */
	@Ignore
	@Test
	public void UnitTestdeleteEmployeeService(){
		//id is exist
		int id = 2;
		assertTrue(employeeServiceImpl.deleteEmployeeService(id));
	}
	@Test
	public void NegativeUnitTestdeleteEmployeeService(){
		//where if id not exist
		int id = 2;
		assertFalse(employeeServiceImpl.deleteEmployeeService(id));
	}
    }
