package com.studentDetails.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.studentDetails.bean.Address;
import com.studentDetails.bean.Student;
import com.studentDetails.util.DBConnectionUtil;

/**
 * this class deals with crud operations on studentdetails
 * 
 * @author SRIKANTH
 *
 */
public class StudentDAOlmpl implements StudentDAO {

	DBConnectionUtil dbconnectionUtil = new DBConnectionUtil();

	/**
	 * this method for inserting new student details
	 * 
	 * @return flag
	 */
	@Override
	public boolean createStudentDetails(Student student, Address address) {
		Connection con = null;
		PreparedStatement ps;
		boolean flag = false;

		// operations on sql query
		String sql = "insert into student_details values(?,?,?,?,?,?,?)";
		try {
			// get the connection
			con = dbconnectionUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, student.getId());
			ps.setString(2, student.getName());
			ps.setInt(3, student.getAge());

			ps.setString(4, address.getCity());
			ps.setString(5, address.getState());
			ps.setString(6, address.getPincode());
			ps.setString(7, student.getPassword());
			int result = ps.executeUpdate();
			if (result > 0) {
				flag = true;
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		// close the connection
		try {
			con.close();
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * this method find the paticular student detals based on id
	 * 
	 * @return studentlist
	 */
	@Override
	public Student findByStudentId(int id) {
		Connection con = null;
		PreparedStatement ps;

		ResultSet rs = null;
		Student student = null;
		Address ad = null;
		// List<Student> studentlist = new <Student>ArrayList();

		// operations on sql query
		String sql = "select * from student_details where s_id = ? ";
		try {
			// get the connection
			con = dbconnectionUtil.getCon();
			ps = con.prepareStatement(sql);

			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {

				ad = new Address();
				ad.setCity(rs.getString(4));
				ad.setState(rs.getString(5));
				ad.setPincode(rs.getString(6));
				student = new Student();

				student.setId(rs.getInt(1));
				student.setName(rs.getString(2));
				student.setAge(rs.getInt(3));
				student.setPassword(rs.getString(7));

				student.setAddress(ad);
				// studentlist.add(student);
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		// close the connection
		try {
			con.close();
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return student;

	}

	/**
	 * this method fetch all students data from database
	 * 
	 * @return studentList
	 */
	@Override
	public List<Student> fetchAllStudents() {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Student st = null;
		Address ad = null;
		List<Student> studentList = new ArrayList<Student>();
		String sql = "select * from student_details";
		try {
			con = dbconnectionUtil.getCon();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ad = new Address();
				ad.setCity(rs.getString(4));
				ad.setState(rs.getString(5));
				ad.setPincode(rs.getString(6));
				st = new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				st.setPassword(rs.getString(7));
				st.setAddress(ad);
				studentList.add(st);
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return studentList;
	}

	/**
    * this method delete the paticular student base on studentid
    * @return flag
    */
	@Override
	public Student deleteStudentDetails(int id) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean flag = false;
		Student student1 = findByStudentId(id);
		String sql = "delete from student_details where s_id = ?";
		
		try {
			//get the connection
			con = dbconnectionUtil.getCon();
            ps = con.prepareStatement(sql);
            ps.setInt(1,id);
            int result = ps.executeUpdate();
            if(result == 1){
            	flag = true;
            }
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		//execute the query
		
		try {
			//close the connection
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return student1;
	}

	/**
     * this method update the paticular student data base on studentId
     */
	@Override
	public Student updateStudentDetails(Student student) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean flag = false;
		Student student1 = null;
		Address ad = null;
		
		String sql = "UPDATE student_details set s_name = ?, s_age= ?,  s_city = ?,  s_state = ?,  s_pincode = ?, s_password = ? WHERE s_id = ?";
		
		
		try {
			con = dbconnectionUtil.getCon();
			
			ps = con.prepareStatement(sql);
			
			ps.setString(1,student.getName());
			ps.setInt(2,student.getAge());
			
			ps.setString(3, student.getAddress().getCity());
			ps.setString(4, student.getAddress().getState());
			ps.setString(5, student.getAddress().getPincode());
			ps.setString(6, student.getPassword());
			ps.setInt(7,student.getId());
			int result = ps.executeUpdate();
			
			if(result >0) {
				flag = true;
				//student1 = findByStudentId(student.getId());
				String sql1 = "select * from student_details where s_id = ? ";
			   // try {
			    	//get the connection
					//con = dbconnectionUtil.getCon();
					ps = con.prepareStatement(sql1);
					
					ps.setInt(1,student.getId());
					rs = ps.executeQuery();
					while(rs.next()) {
						
						ad = new Address();
						ad.setCity(rs.getString(4));
						ad.setState(rs.getString(5));
						ad.setPincode(rs.getString(6));
						student1 = new Student();
						
						student1.setId(rs.getInt(1));
						student1.setName(rs.getString(2));
						student1.setAge(rs.getInt(3));
						student1.setPassword(rs.getString(7));
						
						student1.setAddress(ad);
						//studentlist.add(student);
					}
			    }
			
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		try {
			ps.close();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return student1;
	}
}
