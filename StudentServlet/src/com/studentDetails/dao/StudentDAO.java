package com.studentDetails.dao;

import java.util.List;

import com.studentDetails.bean.Address;
import com.studentDetails.bean.Student;

public interface StudentDAO {
	/**
	 * this method for inserting new student details
	 * @return flag
	 */
     boolean createStudentDetails(Student student,Address address);
     /**
      * this method find the paticular student detals based on id
      * @return studentlist
      */
     Student findByStudentId(int id);
     /**
 	 * this method fetch all students data from database
 	 * @return studentList
 	 */
     List <Student> fetchAllStudents();

     /**
      * this method delete the paticular student base on studentid
      * @return flag
      */
     Student deleteStudentDetails(int id);
     /**
      * this method update the paticular student data base on studentId
      */
     Student  updateStudentDetails(Student student);

}
