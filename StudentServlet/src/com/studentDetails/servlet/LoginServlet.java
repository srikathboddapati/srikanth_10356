package com.studentDetails.servlet;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.studentDetails.bean.Student;
import com.studentDetails.service.StudentService;
import com.studentDetails.service.StudentServicelmpl;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int studentId = Integer.parseInt(request.getParameter("id"));
		String studentPassword = request.getParameter("password");

		StudentService service = new StudentServicelmpl();
		Student student = service.findById(studentId);
		RequestDispatcher rs = request.getRequestDispatcher("show_options.jsp");
		// List<Student> studentList = service.fetchStudents();

		if (student != null) {
			// irator<Student> i= studentList.iterator();

			if (student.getPassword().equals(studentPassword)) {

				// creating session object
				HttpSession session = request.getSession();
				session.setMaxInactiveInterval(10);
				request.setAttribute("student", student);
				// here set the attribute to the session
				session.setAttribute("studentId", studentId);

				String sid = session.getId();
				rs.forward(request, response);
			} else {
				response.sendRedirect("login.html");
			}

		} else {
			response.sendRedirect("login.html");
		}
	}
}
