package com.studentDetails.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.studentDetails.bean.Address;
import com.studentDetails.bean.Student;
import com.studentDetails.service.StudentService;
import com.studentDetails.service.StudentServicelmpl;

/**
 * Servlet implementation class UpdateStudentServlet
 */
@WebServlet("/UpdateStudentServlet")
public class UpdateStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateStudentServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// creating the session object for updatestudentservlet
		HttpSession session = request.getSession(false);

		if (session != null) {
			// get the form values throw request object
			int studentId = Integer.parseInt(request.getParameter("id"));
			String studentPassword = request.getParameter("password");
			String studentName = request.getParameter("username");
			int studentage = Integer.parseInt(request.getParameter("age"));
			String studentCity = request.getParameter("city");
			String studentState = request.getParameter("state");
			String studentPinCode = request.getParameter("pincode");

			// assaign the values to the address object
			Address address = new Address();
			address.setCity(studentCity);
			address.setState(studentState);
			address.setPincode(studentPinCode);

			// assaign values to the student object
			Student student = new Student();
			student.setId(studentId);
			student.setPassword(studentPassword);
			student.setName(studentName);
			student.setAge(studentage);
			student.setAddress(address);

			StudentService studentService = new StudentServicelmpl();
			Student result = studentService.updateStudent(student);
			RequestDispatcher rd = request.getRequestDispatcher("updated_student.jsp");

			// PrintWriter out = response.getWriter();
			// out.print("<html><body");
			if (result != null) {
				request.setAttribute("student", student);
				rd.forward(request, response);

				// out.print("<h2>name of student " + student.getName() + " with
				// " + " age: " + student.getAge() + " details updated</h2>");
				// out.print("<h5>updated all student list");
				// List<Student> updatedList = studentService.fetchStudents();
				// Iterator<Student> i= updatedList.iterator();
				// out.println("<table border= '1'>");
				// out.println("<tr><th>Id</th><th>Name</th><th>age</th>
				// </tr>");
				// while(i.hasNext()) {
				// Student student1 = i.next();
				// out.println("<tr><td>"+student1.getId()+"</td>");
				// out.println("<td>"+student1.getName()+"</td>");
				// out.println("<td>"+student1.getAge()+"</td></tr>");
				// }
				// out.println("</table>");
				//
				// }
				// else {
				// out.print("<h2>student data not inserted error occour</h2>");
				// }
				// out.print("</body></html>");
				// out.close();
			}
		}
		 else {
				response.sendRedirect("login.html");
			}
	}
}
