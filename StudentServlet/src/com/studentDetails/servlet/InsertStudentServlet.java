


package com.studentDetails.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.studentDetails.bean.Address;
import com.studentDetails.bean.Student;
import com.studentDetails.service.StudentService;
import com.studentDetails.service.StudentServicelmpl;

/**
 * Servlet implementation class InsertStudentServlet
 */
@WebServlet("/InsertStudentServlet")
public class InsertStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertStudentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int studentId = Integer.parseInt(request.getParameter("id"));
		String studentPassword = request.getParameter("password");
		String studentName = request.getParameter("username");
		int studentage = Integer.parseInt(request.getParameter("age"));
		String studentCity= request.getParameter("city");
		String studentState = request.getParameter("state");
		String studentPinCode = request.getParameter("pincode");
		
		Address address = new Address();
		address.setCity(studentCity);
		address.setState(studentState);
		address.setPincode(studentPinCode);
		
		Student student = new Student();
		student.setId(studentId);
		student.setPassword(studentPassword);
		student.setName(studentName);
		student.setAge(studentage);
		student.setAddress(address);
		
		
			
		StudentService studentService = new StudentServicelmpl();
		boolean result = studentService.insertStudentData(student,address);
		RequestDispatcher rd = request.getRequestDispatcher("insert_student.jsp");
//		PrintWriter out = response.getWriter();
//		out.print("<html><body>");
		if(result){
			request.setAttribute("student",student );
			rd.forward(request,response);
		}
//			out.print("<h2>name of student " + student.getName() + " with " + " id: " +  student.getId() + " details inserted</h2>");
//		}
//		else{
//			out.print("<h2>student id:" + student.getId() + " is all ready exist " + " try with another id </h2>");
//		}
//		out.print("</body></html>");
//		
		
		
	}

}
