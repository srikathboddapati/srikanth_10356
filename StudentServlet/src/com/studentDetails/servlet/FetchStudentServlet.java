package com.studentDetails.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.studentDetails.bean.Student;
import com.studentDetails.service.StudentService;
import com.studentDetails.service.StudentServicelmpl;

/**
 * Servlet implementation class FetchStudentServlet
 */
@WebServlet("/FetchStudentServlet")
public class FetchStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FetchStudentServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//creating the session object
		HttpSession session = request.getSession(false);
		
       
       if(session != null){
    	String sid = session.getId();
   		//String student_Id = (String)session.getAttribute("studentId");
   		//int user_id = Integer.parseUnsignedInt("student_Id");
            int user_id = (int) session.getAttribute("studentId");
		StudentService service = new StudentServicelmpl();

		List<Student> studentList = service.fetchStudents();
		
	    request.setAttribute("studentList",studentList);
        RequestDispatcher rd = request.getRequestDispatcher("display_all.jsp");
        rd.forward(request, response);
		// how to create the requset dispatcher 
		

		// PrintWriter out = response.getWriter();
		// out.print("<html><body>");
		// if(!studentList.isEmpty()) {
		// Iterator<Student> i= studentList.iterator();
		//
		//
		// out.println("<table border='1'>");
		// out.println("<tr><th>ID</th><th>Name</th><th>Age</th><th>City</th><th>State</th><th>Pincode</th><th>password</th></tr>");
		// while(i.hasNext()) {
		// Student student=i.next();
		// out.println("<tr><td>"+student.getId()+"</td>");
		// out.println("<td>"+student.getName()+"</td>");
		// out.println("<td>"+student.getAge()+"</td>");
		// out.println("<td>"+student.getAddress().getCity()+"</td>");
		// out.println("<td>"+student.getAddress().getState()+"</td>");
		// out.println("<td>"+student.getAddress().getPincode()+"</td>");
		// out.println("<td>"+student.getPassword()+"</td></tr>");
		//
		// }
		// out.println("</table>");
		// }
		// else {
		// out.print("no details are present");
		// }
		// out.print("</body></html>");
		// out.close();
       }else {
			response.sendRedirect("login.html");
		}
	}

}
