package com.studentDetails.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.studentDetails.bean.Student;
import com.studentDetails.service.StudentService;
import com.studentDetails.service.StudentServicelmpl;

/**
 * Servlet implementation class DeleteStudentServelet
 */
@WebServlet("/DeleteStudentServelet")
public class DeleteStudentServelet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteStudentServelet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// creating the session object
		HttpSession session = request.getSession(false);

		if (session != null) {
			String sid = session.getId();
			// String student_Id = (String)session.getAttribute("studentId");
			// int user_id = Integer.parseUnsignedInt("student_Id");
			int user_id = (int) session.getAttribute("studentId");

			// int userId = Integer.parseInt(request.getParameter("id"));
			// String uName = request.getParameter("UserName");
			// Student student = new Student();
			// student.setId(userId);
			// student.setName(uName);
			StudentService studentService = new StudentServicelmpl();
			Student student1 = studentService.deleteStudent(user_id);
			// PrintWriter out = response.getWriter();
			// out.print("<html><body>");
			RequestDispatcher rd = request.getRequestDispatcher("delete_student.jsp");
			// if(result){
			request.setAttribute("student1", student1);
			rd.forward(request, response);
			// out.print("<h2>name of student " + student.getName() + " with " +
			// " id: " + student.getId() + " details ideleated</h2>");
		} else {
			response.sendRedirect("login.html");
		}
		// else{
		// out.print("<h2> studentId id :" + userId + " not present</h2>");
		// }
		// out.print("</body></html>");

	}
}
