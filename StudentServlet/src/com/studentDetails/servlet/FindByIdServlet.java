package com.studentDetails.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.studentDetails.bean.Student;
import com.studentDetails.service.StudentService;
import com.studentDetails.service.StudentServicelmpl;

/**
 * Servlet implementation class FindByIdServlet
 */
@WebServlet("/FindByIdServlet")
public class FindByIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FindByIdServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// creating the session object
		HttpSession session = request.getSession(false);

		if (session != null) {
			String sid = session.getId();
			// String student_Id = (String)session.getAttribute("studentId");
			// int user_id = Integer.parseInt(student_Id);
			int user_id = (int) session.getAttribute("studentId");
			// int user_id = Integer.parseInt(request.getParameter("id"));
			// Student student = new Student();
			StudentService studentService = new StudentServicelmpl();
			Student student1 = studentService.findById(user_id);
			// PrintWriter out = response.getWriter();
			// out.print("<html><body>");

			// creating session object for login id
			// HttpSession session = request.getSession();
			// session.setAttribute("student1", student1);
			// session.setMaxInactiveInterval(1000);
			// String sid = session.getId();
			// this request dispatcher send data from current servlet to display
			// jsp
			RequestDispatcher rd = request.getRequestDispatcher("findbyid.jsp");
			if (student1 != null) {
				if (student1.getId() == user_id) {
					request.setAttribute("student1", student1);
					rd.forward(request, response);

					// response.sendRedirect("home.html");

					// out.println("<tr><td>"+student1.getId()+"</td>");
					// out.println("<td>"+student1.getName()+"</td>");
					// out.println("<td>"+student1.getAge()+"</td>");
					// out.println("<td>"+student1.getAddress().getCity()+"</td>");
					// out.println("<td>"+student1.getAddress().getState()+"</td>");
					// out.println("<td>"+student1.getAddress().getPincode()+"</td>");
					// out.println("<td>"+student1.getPassword()+"</td></tr>");

				}
			}
			// else {

			// out.print("no id present");

			// }
			// out.print("</body></html>");
			// out.close();
		} else {
			response.sendRedirect("login.html");
		}
	}

}
