package com.studentDetails.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 * this class contain the method which deals with connection to the database
 * @author SRIKANTH
 *
 */
public class DBConnectionUtil {
	/**
	 * method loading the driver and create the connection to database
	 * @return connection 
	 */
	public  Connection getCon() {
		Connection con = null;
		
		try {
			//1.loading the driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			System.out.println("driver is loaded");
			
			//2.establish the connection
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/student", "root", "innominds");
			System.out.println("connected to database");
			
		
			
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return con;
	}
	
}
