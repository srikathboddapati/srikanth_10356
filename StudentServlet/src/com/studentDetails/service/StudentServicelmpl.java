package com.studentDetails.service;

import java.util.List;

import com.studentDetails.bean.Address;
import com.studentDetails.bean.Student;
import com.studentDetails.dao.StudentDAO;
import com.studentDetails.dao.StudentDAOlmpl;
/**
 * this class deals with the student operations and get and send the data from dao class
 * @author SRIKANTH
 *
 */
public class StudentServicelmpl implements StudentService{
	StudentDAO studentDAO = new StudentDAOlmpl();
	/**
	 * this method insurting new student data into dao methods
	 * @return boolean 
	 * 
	 */
	@Override
	public boolean insertStudentData(Student student,Address address) {
		boolean result = studentDAO.createStudentDetails(student,address);
		return result;
	}
	/**
	 * this method finding the paticular student data from dao method based on id
	 * @return List
	 * 
	 */
	@Override
	public Student findById(int id) {
		 Student studentList = studentDAO.findByStudentId(id);
		return studentList;
	}
	/**
	 * this method fetch all students data from dao method
	 * @return List
	 * 
	 */
	@Override
	public List<Student> fetchStudents() {
		
		StudentDAO studentDAO = new StudentDAOlmpl();
		List<Student> studentList = studentDAO.fetchAllStudents();
		return studentList;
		
	}
	/**
	 * this method delete the paticular student data from dao method based on id
	 * @return boolean
	 * 
	 */
	@Override
	public Student deleteStudent(int id) {
		Student result = studentDAO.deleteStudentDetails(id);
		return result;
	}
	/**
	 * this method update the paticular student data from dao method
	 * @return boolean
	 * 
	 */
	@Override
	public Student updateStudent(Student student) {
		Student result = studentDAO.updateStudentDetails(student);
		return result;
	}

}
