package com.studentDetails.service;

import java.util.List;

import com.studentDetails.bean.Address;
import com.studentDetails.bean.Student;

public interface StudentService {
	/**
	 * this method insurting new student data into dao methods
	 * @return boolean 
	 * 
	 */
	boolean insertStudentData(Student student,Address address);
	/**
	 * this method finding the paticular student data from dao method based on id
	 * @return List
	 * 
	 */
	public Student findById(int id);
	/**
	 * this method fetch all students data from dao method
	 * @return List
	 * 
	 */
	public List<Student> fetchStudents();
	/**
	 * this method delete the paticular student data from dao method based on id
	 * @return boolean
	 * 
	 */
	public Student deleteStudent(int id);
	/**
	 * this method update the paticular student data from dao method
	 * @return boolean
	 * 
	 */
	public Student updateStudent(Student student);

}
