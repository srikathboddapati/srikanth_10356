import React, { Component } from 'react';
import './Footer.css';
class Footer extends Component {
  render() {
    return (
      <div className="container-fluid">
      <div>
        <footer style={{display: 'block'}}>
          <div className="privacy-policy-footer">
            <h4>Privacy Note</h4>
            <p>By using www.bookmyshow.com(our website), you are fully accepting the Privacy Policy available at <a href="/privacy">https://bookmyshow.com/privacy</a> governing your access to Bookmyshow and provision of services by Bookmyshow to you. If you do not accept terms mentioned in the <a href="/privacy">Privacy Policy</a>, you must not share any of your personal information and immediately exit Bookmyshow.</p>
          </div>

          <div className = "footermiddle"> 
          {/* Supplimentary action start here */}
          <div className="container-fluid">
          <div className="row sup-action ">
          <div className="col-md-4">
              <a className="col sup-action-container" href="">
                <div className="__sup-icon">
                  <span>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" enableBackground="new 0 0 100 100" xmlSpace="preserve">
                      <use xlinkHref="/icons/common-icons.svg#icon-callcenter" />
                       <svg xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 102 102" id="icon-callcenter"><path d="M68.8 29.4c0-10.4-8.4-18.8-18.8-18.8S31.2 19 31.2 29.4 39.6 48.2 50 48.2s18.8-8.4 18.8-18.8zm-35.8 0c0-9.3 7.6-17 17-17 9.3 0 17 7.6 17 17 0 9.3-7.6 17-17 17-9.3 0-17-7.6-17-17z" /><path d="M24 30.3c.5 0 .9-.4.9-.9C24.9 15.6 36.2 4.3 50 4.3s25.1 11.3 25.1 25.1c0 13.1-10.5 28.3-24.2 29v-3.9c0-.5-.4-.9-.9-.9s-.9.4-.9.9v8.6c0 .5.4.9.9.9s.9-.4.9-.9v-2.9c14.7-.7 26-16.8 26-30.8C76.9 14.6 64.8 2.5 50 2.5S23.1 14.6 23.1 29.4c0 .5.4.9.9.9z" /><path d="M68.1 63c-.6-.2-1.2-.3-1.9-.3-.9 0-2.3.7-4.9 2.1-3.2 1.7-7.7 4-10.6 4-3.2 0-8-2.3-11.5-4-2.8-1.3-4.3-2-5.3-2-.7 0-1.3.1-1.9.3C16.7 68.2 11.2 86 11.2 96.7c0 .5.4.9.9.9H88c.5 0 .9-.4.9-.9 0-10.8-5.6-28.6-20.8-33.7zM13 95.7c.3-12.1 7.2-26.8 19.6-30.9.4-.1.8-.2 1.3-.2.6 0 2.7 1 4.5 1.9 3.9 1.9 8.7 4.2 12.3 4.2 3.3 0 7.8-2.3 11.4-4.2 1.6-.8 3.6-1.9 4.1-1.9s.9.1 1.3.2C79.9 68.9 86.7 83.6 87 95.7H13zm3.6-55.3c.5 0 .9-.4.9-.9V26.3c0-.5-.4-.9-.9-.9s-.9.4-.9.9v13.2c0 .5.4.9.9.9zm66.8 0c.5 0 .9-.4.9-.9V26.3c0-.5-.4-.9-.9-.9s-.9.4-.9.9v13.2c-.1.5.4.9.9.9z" /></svg>
                    </svg>
                  </span><br />
                  <p className="__text">24/7 CUSTOMER CARE</p>
                </div>
              </a>
              </div>

              <div className="col-md-4">
              <a className="col sup-action-container" href="">
                <div className="__sup-icon">
                  <span>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" enableBackground="new 0 0 100 100" xmlSpace="preserve">
                      <use xlinkHref="/icons/common-icons.svg#icon-resend" />
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 102 102" id="icon-resend" width="100%" height="100%"><path d="M92.1 69l-5.6-13.9a1 1 0 0 0-1.1-.5c-1.9.8-4.1-.2-4.8-2.1-.4-.9-.4-1.9 0-2.8.4-.9 1.1-1.6 2-2 .2-.1.4-.2.5-.4s.1-.4 0-.6l-5.6-13.9a1 1 0 0 0-1.1-.5L61 38.5l9-20.9c.2-.4 0-.9-.4-1.1L57 11.1c-.4-.2-.9 0-1.1.4a3.3 3.3 0 0 1-4.3 1.8c-.8-.3-1.4-1-1.8-1.8-.3-.8-.3-1.7 0-2.5.1-.2.1-.4 0-.6-.1-.2-.2-.4-.4-.5L36.8 2.6c-.4-.2-.9 0-1.1.4L12.3 58.2l-3.9 1.5c-.2.2-.4.3-.4.5-.1.2-.1.4 0 .6l5.6 13.9c.2.4.7.6 1.1.5 1.9-.7 4.1.2 4.8 2 .4.9.4 1.9 0 2.8s-1.1 1.6-2 2c-.2.1-.4.2-.5.4-.1.2-.1.4 0 .6l5.4 14c.1.2.2.4.4.5.1 0 .2.1.3.1.1 0 .2 0 .3-.1l68.1-27.3c.5-.2.7-.7.6-1.2zM36.9 4.4L48 9.1c-.2 1-.2 2 .2 3 .5 1.2 1.4 2.2 2.7 2.7 2.3 1 4.8.1 6.1-1.9l11.1 4.7-9.3 21.8-4.7 1.9L59 30.2l2.5-5.8c.1-.2.1-.4 0-.6s-.2-.4-.4-.5L38 13.5c-.2-.1-.4-.1-.6 0-.2.1-.4.2-.5.4L19.1 55.5l-4.7 1.9 22.5-53zm-1.4 16.7l.8.3c.1 0 .2.1.3.1.3 0 .6-.2.8-.5.2-.4 0-.9-.4-1.1l-.8-.3 1.8-4.3 21.6 9.2-1.8 4.3-.8-.4c-.4-.2-.9 0-1.1.4-.2.4 0 .9.4 1.1l.8.3-5.1 12-30.7 12.4 14.2-33.5zM23.7 95.6l-5-12.4c1-.6 1.7-1.4 2.2-2.5.6-1.3.6-2.8 0-4.1-.8-2-2.8-3.4-5-3.4-.4 0-.8 0-1.2.1L9.8 61l66.5-26.7 5 12.4c-1 .6-1.7 1.4-2.2 2.5a4.9 4.9 0 0 0 0 4.1c.8 2 2.8 3.4 5 3.4.4 0 .8 0 1.2-.1l5 12.4-66.6 26.6z" /><path d="M70 42.1c-.1-.2-.2-.4-.4-.5-.2-.1-.4-.1-.6 0L20.2 61.2a1 1 0 0 0-.5 1.1L30 87.8c.1.2.2.4.4.5.1 0 .2.1.3.1.1 0 .2 0 .3-.1l42.3-17 6.4-2.6c.4-.2.6-.7.5-1.1L70 42.1zm3.5 27.3l-.3-.8a1 1 0 0 0-1.1-.5 1 1 0 0 0-.5 1.1l.4.8-34.2 13.7-.3-.7a1 1 0 0 0-1.1-.5 1 1 0 0 0-.5 1.1l.3.8-5.1 2-9.6-23.9 5.1-2 .3.8c.1.3.4.5.8.5.1 0 .2 0 .3-.1.4-.2.6-.7.5-1.1l-.3-.8 34.1-13.7.3.8c.1.3.4.5.8.5.1 0 .2 0 .3-.1.4-.2.6-.7.5-1.1l-.3-.8 4.9-2 9.6 23.9-4.9 2.1z" /><path d="M30.6 65.7c-.2-.4-.7-.6-1.1-.5s-.6.7-.5 1.1l1.4 3.4c.1.3.4.5.8.5.1 0 .2 0 .3-.1.4-.2.6-.7.5-1.1l-1.4-3.3zm3.5 8.6a1 1 0 0 0-1.1-.5 1 1 0 0 0-.5 1.1l1.4 3.4c.1.3.4.5.8.5.1 0 .2 0 .3-.1.4-.2.6-.7.5-1.1l-1.4-3.3zm32.2-22.9a1 1 0 0 0-1.1-.5 1 1 0 0 0-.5 1.1l1.4 3.4c.1.3.4.5.8.5.1 0 .2 0 .3-.1.4-.2.6-.7.5-1.1l-1.4-3.3zm1.9 9.3l1.4 3.4c.1.3.4.5.8.5.1 0 .2 0 .3-.1.4-.2.6-.7.5-1.1L69.7 60a1 1 0 0 0-1.1-.5c-.4.2-.6.7-.4 1.2zm-19.5-34l3.1 1.3c.1 0 .2.1.3.1.3 0 .6-.2.8-.5.2-.4 0-.9-.4-1.1l-3.1-1.3c-.4-.2-.9 0-1.1.4-.3.4-.1.9.4 1.1zm-7.8-3.3l3.1 1.3c.1 0 .2.1.3.1.3 0 .6-.2.8-.5.2-.4 0-.9-.4-1.1l-3.1-1.3c-.4-.2-.9 0-1.1.4-.2.4 0 .9.4 1.1zm13.4 41.4c-.5 0-.8.4-.8.8 0 2.4-2 4.4-4.4 4.4s-4.4-2-4.4-4.4c0-2.3 1.9-4.2 4.2-4.3l-1.6 2.3c-.3.4-.2.9.2 1.2.1.1.3.1.5.1.3 0 .5-.1.7-.4l2.6-3.7c.3-.4.2-.9-.2-1.1L47.5 57c-.4-.3-.9-.2-1.2.2s-.2.9.2 1.2l1.9 1.4c-3 .3-5.4 2.9-5.4 5.9 0 3.3 2.7 6 6 6s6-2.7 6-6c.1-.5-.3-.9-.7-.9z" /></svg>
                    </svg>
                  </span><br />
                  <p className="__text">RESEND BOOKING CONFIRMATION</p>
                </div>
              </a>
              </div>

                <div className="col-md-4">
              <a className="col sup-action-container" href="">
                <div className="__sup-icon">
                  <span>
                     <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" enableBackground="new 0 0 100 100" xmlSpace="preserve">
                      <use xlinkHref="/icons/common-icons.svg#icon-newsletter" />
                       <svg xmlns="http://www.w3.org/2000/svg" id="icon-newsletter" viewBox="-1 -1 102 102" width="100%" height="100%"><g id="eknewsletter"><path className="ekst0" d="M97.5 90.3V33.6c0-.5-.4-1-1-1H82.9V9.8c0-.5-.4-1-1-1H18.1a1 1 0 0 0-1 1v22.8H3.5a1 1 0 0 0-1 1v56.9c0 .1.1.1.1.2v.1c0 .1.1.1.2.1.1.1.2.1.2.1.1 0 .1 0 .2.1h93.4c.1 0 .3 0 .4-.1 0 0 .1 0 .1-.1.1-.1.2-.1.2-.2v-.1c0-.1.1-.1.1-.2.1 0 .1 0 .1-.1zm-1.9-55.7v53.6L63.9 64.1l18.6-17.7 7.7-7.4c.3-.3.4-.7.2-1.1-.1-.4-.5-.6-.9-.6h-6.8v-2.7h12.9zM50.2 56.1l43.4 33.1H6.4l43.8-33.1zm32.7-16.8h4.4l-4.4 4.1v-4.1zM19.1 10.8h61.8v34.5L62.4 62.9l-11.6-8.8c-.3-.3-.8-.3-1.2 0L37.8 63 19.1 45.3V10.8zm-2 32.6l-4.4-4.1h4.4v4.1zm0-8.8v2.7h-6.8c-.4 0-.8.2-.9.6-.1.4-.1.8.2 1.1l26.5 25.2-31.8 24V34.6h12.8z" /><path className="ekst0" d="M28.9 22.5h42.7a1 1 0 0 0 0-2H28.9c-.5 0-1 .4-1 1s.4 1 1 1zm0 10h42.7a1 1 0 0 0 0-2H28.9a1 1 0 0 0 0 2zm0 9.9h42.7a1 1 0 0 0 0-2H28.9c-.5 0-1 .4-1 1s.4 1 1 1z" /></g></svg>
                    </svg>
                  </span><br />
                  <p className="__text">RESEND BOOKING CONFIRMATION</p>
                </div>
              </a>
              </div>

              
             
            
          </div>
          </div>
          {/* Supplimentary action Ends here */}
          {/* Footer links */}

            <div className="container-fluid new-footer-links">
              <div className="row wrapper">
                <div className="new-movies-links">
                  <ul className="footer_movies">
                    <li>
                      <a href="" title="Online Movie Ticket Booking Mumbai">
                        <span>Online Movie Ticket Booking Mumbai </span>
                        <span className="seprator"> | </span>
                      </a>
                    </li>
                    <li>
                      <a href="" title="Online Movie Ticket Booking Delhi">
                        <span>Online Movie Ticket Booking Delhi</span>
                        <span className="seprator"> | </span>
                      </a>
                    </li>
                    <li>
                      <a href="" title="Online Movie Ticket Booking Bengaluru">
                        <span>Online Movie Ticket Booking Bengaluru</span>
                        <span className="seprator"> | </span>
                      </a>
                    </li>
                    <li>
                      <a href="" title="Online Movie Ticket Booking Hyderabad">
                        <span>Online Movie Ticket Booking Hyderabad</span>
                        <span className="seprator"> | </span>
                      </a>
                    </li>
                    <li>
                      <a href="" title="Online Movie Ticket Booking Chennai">
                        <span>Online Movie Ticket Booking Chennai</span>
                        <span className="seprator"> | </span>
                      </a>
                    </li>
                    <li>
                      <a href="" title="Online Movie Ticket Booking Kolkata">
                        <span>Online Movie Ticket Booking Kolkata</span>
                        <span className="seprator"> | </span>
                      </a>
                    </li>
                    <li>
                      <a href="" title="Online Movie Ticket Booking Noida">
                        <span>Online Movie Ticket Booking Noida</span>
                        <span className="seprator"> | </span>
                      </a>
                    </li>
                    <li>
                      <a href="" title="Online Movie Ticket Booking Ahmedabad">
                        <span>Online Movie Ticket Booking Ahmedabad</span>
                        <span className="seprator"> | </span>
                      </a>
                    </li>
                    <li>
                      <a href="" title="Online Movie Ticket Booking Pune">
                        <span>Online Movie Ticket Booking Pune</span>
                      </a>
                    </li>
                  </ul>
                </div>
                </div>
            </div>
               
              <div className="bestmovie">
              
               <div className="container-fluid">
                <div className="row">
                  <div className="col-md-3 movies-by-genre m1">
                    <h4>BEST LANGUAGE GENRE MOVIES</h4>
                    <ul className="by-genre-column">
                      <li>
                        <a href="" title="Hindi Comedy Movies">
                          <span>Hindi Comedy Movies</span><span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Hindi Action Movies">
                          <span>Hindi Action Movies</span><span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="English Animated Movies">
                          <span>English Animated Movies</span><span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="English Fantasy Movies">
                          <span>English Fantasy Movies</span><span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="English Comedy Movies">
                          <span>English Comedy Movies</span><span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="English Action Movies">
                          <span>English Action Movies</span><span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Tamil Action Movies">
                          <span>Tamil Action Movies</span><span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Tamil Comedy Movies">
                          <span>Tamil Comedy Movies</span><span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Telugu Action Movies">
                          <span>Telugu Action Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Gujarati Comedy Movies">
                          <span>Gujarati Comedy Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Marathi Drama Movies">
                          <span>Marathi Drama Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Marathi Comedy Movies">
                          <span>Marathi Comedy Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Hindi Animated Movies">
                          <span>Hindi Animated Movies</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="col-md-3 movies-by-genre m1">
                    <h4>MOVIES BY GENRE </h4>
                    <ul className="by-genre-column">
                      <li>
                        <a href="" title="Best Action Movies">
                          <span>Best Action Movies</span><span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best  Romantic Movies">
                          <span>Best  Romantic Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best Comedy Movies">
                          <span>Best Comedy Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best Adult Movies">
                          <span>Best Adult Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best Adventure Movies">
                          <span>Best Adventure Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best Biography Movies">
                          <span>Best Biography Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best Classic Movies">
                          <span>Best Classic Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best Crime Movies">
                          <span>Best Crime Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best Drama Movies">
                          <span>Best Drama Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best Family Movies">
                          <span>Best Family Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best Fantasy Movies">
                          <span>Best Fantasy Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best History Movies">
                          <span>Best History Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best Musical Movies">
                          <span>Best Musical Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best Mystery Movies">
                          <span>Best Mystery Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best Sci-Fi Movies">
                          <span>Best Sci-Fi Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best Suspense Movies">
                          <span>Best Suspense Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best Thriller Movies">
                          <span>Best Thriller Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Best  Western Movies">
                          <span>Best  Western Movies</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="col-md-3 movies-by-language m1">
                    <h4>POPULAR MOVIES BY LANGUAGE</h4>
                    <ul className="by-language-column">
                      <li>
                        <a href="" title="Popular Hindi Movies">
                          <span>Popular Hindi Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Popular English Movies">
                          <span>Popular English Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Popular Marathi Movies">
                          <span>Popular Marathi Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Popular Bengali Movies">
                          <span>Popular Bengali Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Popular Tamil Movies">
                          <span>Popular Tamil Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Popular Telugu Movies">
                          <span>Popular Telugu Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Popular Malayalam Movies">
                          <span>Popular Malayalam Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Popular Bhojpuri Movies">
                          <span>Popular Bhojpuri Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Popular Kannada Movies">
                          <span>Popular Kannada Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Popular Punjabi Movies">
                          <span>Popular Punjabi Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Popular Gujarati Movies">
                          <span>Popular Gujarati Movies</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" title="Popular Sindhi Movies">
                          <span>Popular Sindhi Movies</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="col-md-3 movies-reviews m1">
                    <h4>MOVIE REVIEWS</h4>
                    <ul className="groups">
                      <li>
                        <a href="" rel="noopener noreferrer" target="_blank" title="Trending Articles">
                          <span>Trending Articles</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" rel="noopener noreferrer" target="_blank" title="Latest News on Movies, Events, Plays & Sports">
                          <span>Latest News on Movies, Events, Plays &amp; Sports</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" rel="noopener noreferrer" target="_blank" title="Rajeev Masand Movie Reviews">
                          <span>Rajeev Masand Movie Reviews</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" rel="noopener noreferrer" target="_blank" title="Anupama Chopra Movie Review">
                          <span>Anupama Chopra Movie Review</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" rel="noopener noreferrer" target="_blank" title="Film Reviews">
                          <span>Film Reviews</span>
                          <span className="seprator"> | </span>
                        </a>
                      </li>
                      <li>
                        <a href="" rel="noopener noreferrer" target="_blank" title="Guest Blogging">
                          <span>Guest Blogging</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                  
                

                

      
        <div className="col-md-3 m1">
          <div className="col latest-movies" id="video">
            <h4>VIDEOS</h4>
            <ul className="groups">
              <li>
                <a href="" title="Trailers">
                  <span>Trailers</span>
                  <span className="seprator"> | </span>
                </a>
              </li>
              <li>
                <a href="" title="MyBollywood">
                  <span>MyBollywood</span>
                  <span className="seprator"> | </span>
                </a>
              </li>
              <li>
                <a href="" title="MyTV">
                  <span>MyTV</span>
                  <span className="seprator"> | </span>
                </a>
              </li>
              <li>
                <a href="" title="MyStyle">
                  <span>MyStyle </span>
                </a>
              </li>
            </ul>
          </div>
          <div className="col" id="events-calender">
          </div>
        </div>

        <div className="col-md-3 movie-trailers m1">
          <h4>EVENTS TICKETS BOOKING ONLINE</h4>
          <ul className="groups">
            <li><a href="" title="Upcoming Events in Mumbai"><span>Upcoming Events in Mumbai</span><span className="seprator"> | </span></a></li>
            <li><div style={{wordWrap: 'break-word'}}><a href="/delhi/events" title="Upcoming Events in Delhi"><span>Upcoming Events in Delhi</span><span className="seprator"> | </span></a></div></li>
            <li><a href="" title="Upcoming Events in Bengaluru"><span>Upcoming Events in Bengaluru</span><span className="seprator"> | </span></a></li>
            <li><a href="" title="Upcoming Events in Chennai"><span>Upcoming Events in Chennai</span><span className="seprator"> | </span></a></li>
            <li><a href="" title="Upcoming Events in Hyderabad"><span>Upcoming Events in Hyderabad</span><span className="seprator"> | </span></a></li>
            <li><a href="" title="Upcoming Events in Pune"><span>Upcoming Events in Pune</span><span className="seprator"> | </span></a></li>
            <li><a href="" title="Upcoming Events in Noida"><span>Upcoming Events in Noida</span><span className="seprator"> | </span></a></li>
            <li><a href="" title="Upcoming Events in Ahmedabad"><span>Upcoming Events in Ahmedabad</span></a></li>
          </ul>
        </div>
        <div className="col-md-3 cinemas-and-regions m1">
          <h4>MOVIES, CINEMAS &amp; CELEBRITY </h4>
          <ul className="groups">
            <li><a href="" title="Latest Upcoming Movies"><span>Latest Upcoming Movies</span><span className="seprator"> | </span></a></li>
            <li><a href="" title="Best Now Showing Movies"><span>Best Now Showing Movies</span><span className="seprator"> | </span></a></li>
            <li><a href="" title="Cinemas &amp; Theatres"><span>Cinemas &amp; Theatres</span><span className="seprator"> | </span></a></li>
            <li><a href="" title=" Movie Stars &amp; Celebrities"><span> Movie Stars &amp; Celebrities</span><span className="seprator"> | </span></a></li>
            <li><a href="" title="Cinema Chains"><span>Cinema Chains</span><span className="seprator"> | </span></a></li>
            <li><a href="" title="All regions Cinemas"><span>All regions Cinemas</span></a></li>
          </ul>
        </div>
        <div className="col-md-3 m1">
          <div className="col movie-trailers" id="movie-trailers">
            <h4>ARCHIVED COLLECTION </h4>
            <ul className="groups">
              <li><a href="" title="Old Movies"><span>Old Movies</span><span className="seprator"> | </span></a></li>
              <li><a href="" title="Past Events"><span>Past Events</span><span className="seprator"> | </span></a></li>
              <li><a href="" title="Classic Sports Events"><span>Classic Sports Events</span><span className="seprator"> | </span></a></li>
              <li><a href="" title="Old Theatre Plays"><span>Old Theatre Plays</span><span className="seprator"> | </span></a></li>
              <li><a href=""><span>Best Movies 2016</span><span className="seprator"> | </span></a></li>
              <li><a href=""><span>Best Of 2017</span><span className="seprator"> | </span></a></li>
              <li><a href=""><span>Best Of 2018</span></a></li>
            </ul>
            </div>
          </div>
          <div className="col-md-3 m1" id="country">
            <h4>COUNTRIES</h4>
            <ul className="groups">
               <li><a href="" rel="noopener noreferrer" target="_blank" title="Bangladesh"><span>Bangladesh</span><span className="seprator"> | </span></a></li> 
              <li><a href="" rel="noopener noreferrer" target="_blank" title="Indonesia"><span>Indonesia</span><span className="seprator"> | </span></a></li>
              <li><a href="" rel="noopener noreferrer" target="_blank" title="New Zealand"><span>New Zealand</span><span className="seprator"> | </span></a></li>
              <li><a href="" rel="noopener noreferrer" target="_blank" title="UAE"><span>UAE</span><span className="seprator"> | </span></a></li>
              <li><a href="" rel="noopener noreferrer" target="_blank" title="Sri Lanka"><span>Sri Lanka</span><span className="seprator"> | </span></a></li>
              <li><a href="" rel="noopener noreferrer" target="_blank" title="West Indies"><span>West Indies</span></a></li>
            </ul>
            </div>
            </div>
          </div>
        </div>
         
         <div className="container-fluid"> 
        <div className="row footerend">
           <hr style={{color:"black" }}/>
            <div className="__footer-bms-logo">
                
              <span>
               <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" enableBackground="new 0 0 100 100" xmlSpace="preserve">
                <use xlinkHref="/icons/common-icons.svg#icon-bms" />
                 <svg xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 102 102" id="icon-bms" width="100%" height="100%"><path fill="#D72027" d="M51.4 45.6l-1.2-2.1-2.2 1-1.2-2.1-2.2 1-1.2-2.1-2.2 1-1.2-2.1-2.2 1-1.2-2.1-2.2 1L29.8 55l5.6 1.7a3 3 0 0 1 2.8-2.1 3 3 0 0 1 3 3v.1c0 .3 0 .5-.1.8l5.7 1.7 4.6-14.6z" /><path fill="#FFF" d="M34.7 54.2c-.3-.1-.5-.3-.5-.6v-7.3c0-.3.3-.6.6-.6h5.7c1.1.2 2 1.2 2 2.3v5.6c0 .2-.1.3-.2.4-.1.1-.3.2-.4.2h-.1c-.3-.1-.5-.3-.5-.6V48c0-.6-.4-1-1-1.1H39v6.7c0 .2-.1.3-.2.4-.1.1-.3.2-.4.2h-.1c-.3-.1-.5-.3-.5-.6v-6.7h-2.3v6.7c0 .2-.1.3-.2.4-.1.1-.3.2-.4.2h-.2m8.8 3.2c-.3-.1-.5-.3-.5-.6 0-.2.1-.3.2-.4.1-.1.3-.2.4-.2.5 0 1-.3 1.1-.8l.6-2.1-2.1-6.9c-.1-.3.1-.7.4-.8h.3c.2 0 .4.2.5.4l1.5 5.1 1.5-5.1c.1-.3.4-.5.7-.4.3.1.5.4.4.8l-2.1 7-.7 2.3c-.2.5-.5.9-.9 1.2-.4.3-.9.5-1.4.5h.1m-38-3.3c-.3-.1-.5-.3-.5-.6v-11c0-.2.1-.3.2-.5.1-.1.3-.2.5-.2h.1c.2.2.5.4.5.7v3.1h1.6c1.2.2 2 1.2 2 2.4v3.5c0 1.4-1.1 2.5-2.5 2.5H5.6c0 .1 0 .1-.1.1m.8-1.3h1.2c.7 0 1.2-.5 1.2-1.2v-3.5c0-.6-.4-1.1-1-1.2H6.3v5.9zm7.3 1.3c-1.2-.2-2-1.2-2-2.4V48c0-.7.3-1.3.7-1.7.5-.5 1.1-.7 1.7-.7h.4c1.2.2 2 1.2 2 2.4v3.7c0 .7-.3 1.3-.7 1.7-.5.5-1.1.7-1.7.7-.1.1-.3.1-.4 0m.6-7.2H14c-.7 0-1.2.5-1.2 1.2v3.7c0 .6.4 1.1 1 1.2h.2c.7 0 1.2-.5 1.2-1.2v-3.7c0-.6-.4-1.1-1-1.2m5.9 7.2c-1.2-.2-2-1.2-2-2.4V48c0-1.4 1.1-2.5 2.5-2.5h.4c1.2.2 2 1.2 2 2.4v3.7c0 .7-.3 1.3-.7 1.7-.5.5-1.1.7-1.7.7-.2.2-.3.2-.5.1m.7-7.2h-.2c-.7 0-1.2.5-1.2 1.2v3.7c0 .6.4 1.1 1 1.2h.2c.3 0 .6-.1.8-.3.2-.2.4-.5.4-.9V48c0-.5-.4-1-1-1.1m4.4 7.3c-.3-.1-.5-.3-.5-.6v-11c0-.2.1-.3.2-.5.1-.1.3-.2.5-.2h.1c.3.1.5.3.5.6V49l2.9-3.3c.1-.2.4-.2.6-.2.1 0 .2.1.3.1.3.2.3.6 0 .9l-2.2 2.4 2.3 4.2c.1.1.1.3.1.5s-.2.3-.3.4c-.1.1-.3.1-.4.1-.2 0-.3-.1-.4-.3L26.8 50l-.9 1v2.6c0 .2-.1.3-.2.5-.1.1-.3.2-.4.2l-.1-.1m27.9 0c-.7-.1-1.3-.5-1.6-1.2-.1-.1-.1-.3-.1-.5s.1-.3.3-.4c.1-.1.3-.1.4-.1.2 0 .3.1.4.3.2.3.4.5.8.5.4.1.7 0 .9-.3.2-.2.4-.6.3-.9 0-.3-.2-.6-.5-.8l-1.7-1.2c-.6-.4-.9-1-1-1.7 0-.7.2-1.4.7-1.8.5-.5 1.2-.7 1.9-.6.7.1 1.3.5 1.6 1.1.1.1.1.3.1.5s-.1.3-.3.4c-.1.1-.3.1-.4.1-.2 0-.4-.1-.4-.3-.1-.3-.4-.4-.7-.5-.3-.1-.6 0-.9.3-.2.2-.3.5-.3.8 0 .3.2.6.4.8l1.7 1.2c.6.4 1 1.1 1 1.8.1.7-.2 1.4-.8 1.9-.3.4-1 .7-1.8.6zm4.9 0c-.3-.1-.5-.3-.5-.6v-11c0-.2.1-.3.2-.5.1-.1.3-.2.4-.2h.1c.3.1.5.3.5.6v3.1h1.6c1.2.2 2 1.2 2 2.4v5.4c0 .4-.3.6-.6.6h-.1c-.3-.1-.5-.3-.5-.6V48c0-.6-.4-1.1-1-1.2h-1.4v6.6c0 .4-.3.6-.6.6 0 .2-.1.2-.1.2m8-.1c-1.2-.2-2-1.2-2-2.4V48c0-1.4 1.1-2.5 2.5-2.5h.4c1.2.2 2 1.2 2 2.4v3.7c0 .7-.3 1.3-.7 1.7-.5.5-1.1.7-1.7.7-.2.2-.3.2-.5.1m.7-7.2h-.2c-.3 0-.6.1-.8.3-.2.2-.3.5-.3.8v3.7c0 .6.4 1.1 1 1.2h.2c.3 0 .6-.1.8-.3.2-.2.4-.5.4-.9V48c-.1-.5-.5-1-1.1-1.1m12-.5c0-.2 0-.3-.1-.5-.1-.1-.2-.2-.4-.3h-.1c-.3 0-.5.2-.6.5 0 0-.7 2.8-1.2 4.9-.5-2.1-1.2-4.8-1.2-4.9-.1-.2-.2-.4-.5-.5h-.1c-.3 0-.5.2-.6.5 0 0-.7 2.8-1.2 4.9-.5-2.1-1.2-4.8-1.2-4.9-.1-.2-.3-.4-.5-.5h-.3c-.3.1-.5.4-.5.7l1.8 7.3c.1.2.3.4.5.5h.1c.3 0 .5-.2.6-.5 0 0 .7-2.8 1.2-4.9.5 2.1 1.2 4.9 1.2 4.9.1.2.3.4.5.5h.1c.3 0 .5-.2.6-.5l1.9-7.2z" /></svg>
          </svg>
        </span>
     </div>
      </div>
      </div>

      <div className="fonticons">
      <ul>
      <li>
      <i className="fab fa-facebook-f"></i></li>
       <li><i className="fab fa-twitter"></i></li>
       <li><i className="fab fa-facebook-f"></i></li>
       <li><i className="fab fa-youtube"></i></li>
       <li><i className="fab fa-pinterest-p"></i></li>
       <li><i className="fab fa-google-plus-g"></i></li>
       <li><i className="fab fa-linkedin-in"></i></li>


       </ul>

      </div>
      <div className="row">
        <div className = "copyright">
           <p>Copyright 2019 © Bigtree Entertainment Pvt. Ltd. All Rights Reserved.<br />
          The content and images used on this site are copyright protected and copyrights vests with the respective owners. The usage of the content and images on this website is intended to promote the works and no endorsement of the artist shall be implied.  Unauthorized use is prohibited and punishable by law.</p>
      </div> 
      </div>   
      </div>         
        </footer>

      </div>
    </div>
    );
  }
}

export default Footer;
