import React, { Component } from 'react';
import './Body.css';

class body extends Component {
   render() {
      return (

         <div className="">
            <div className="showcase" id="showcase-primary" data-ratio>
               <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                  <ol className="carousel-indicators">
                     <li data-target="#carouselExampleIndicators" data-slide-to={0} className="active" />
                     <li data-target="#carouselExampleIndicators" data-slide-to={1} />
                     <li data-target="#carouselExampleIndicators" data-slide-to={2} />
                     <li data-target="#carouselExampleIndicators" data-slide-to={3} />
                     <li data-target="#carouselExampleIndicators" data-slide-to={4} />
                  </ol>
                  <div className="carousel-inner">
                     <div className="carousel-item active">
                        <img className="d-block w-100" src="https://in.bmscdn.com/showcaseimage/eventimage/bombairiya-15-01-2019-04-36-27-979.jpg" alt="First slide" />
                     </div>
                     <div className="carousel-item">
                        <img className="d-block w-100" src="https://in.bmscdn.com/showcaseimage/eventimage/fraud-saiyaan-16-01-2019-12-30-10-404.jpg" alt="Second slide" />
                     </div>
                     <div className="carousel-item">
                        <img className="d-block w-100" src="https://in.bmscdn.com/showcaseimage/eventimage/72-hours-martyr-who-never-died-15-01-2019-04-14-23-376.jpg" alt="Third slide" />
                     </div>
                     <div className="carousel-item">
                        <img className="d-block w-100" src="https://in.bmscdn.com/showcaseimage/eventimage/uri--the-surgical-strike-07-01-2019-05-25-17-036.jpg" alt="Third slide" />
                     </div>
                     <div className="carousel-item">
                        <img className="d-block w-100" src="https://in.bmscdn.com/showcaseimage/eventimage/72-hours-martyr-who-never-died-15-01-2019-04-14-23-376.jpg" alt="Third slide" />
                     </div>
                  </div>
                  <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                     <span className="carousel-control-prev-icon" aria-hidden="true" />
                     <span className="sr-only">Previous</span>
                  </a>
                  <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                     <span className="carousel-control-next-icon" aria-hidden="true" />
                     <span className="sr-only">Next</span>
                  </a>
               </div>
            </div>

            <div className="container-fluid list-of-movies">
               <div className="row">
                  <div className="col-md-3">
                     <div className="imgblock">
                        <div className="img">

                        </div>
                     </div>
                     <div className="TrendingSearches">
                        <h4>Trending Searches</h4>
                        <div className="movie">
                           <div className="movieees">
                              <h4>f2</h4>
                           </div>
                           <div className="movieees1">
                              <p>Movies</p>
                           </div>
                        </div>

                        <div className="movie">
                           <div className="movieees">
                              <h4>Mr.Majnu</h4>
                           </div>
                           <div className="movieees1">
                              <p>Movies</p>
                           </div>
                        </div>

                        <div className="movie">
                           <div className="movieees">
                              <h4>URI-The Surgical Strike</h4>
                           </div>
                           <div className="movieees1">
                              <p>Movies</p>
                           </div>
                        </div>


                        <div className="movie">
                           <div className="movieees">
                              <h4>KGF</h4>
                           </div>
                           <div className="movieees1">
                              <p>Movies</p>
                           </div>
                        </div>

                        <div className="movie">
                           <div className="movieees">
                              <h4>peta</h4>
                           </div>
                           <div className="movieees1">
                              <p>Movies</p>
                           </div>
                        </div>





                     </div>





                  </div>
                  <div className="col-md-9">

                     <div className="row movieshead">
                        <div className="col-md-6">
                           <h4>Recommended Movies</h4>
                        </div>
                        <div className="col-md-6">
                           <span className="float-right">viewall</span>
                        </div>
                     </div>

                     <div className="row movies">
                        <div className="col-md-4">
                           <div className="card">
                              <img className="card-img-top" src="//in.bmscdn.com/iedb/movies/images/mobile/thumbnail/xlarge/petta-et00083530-08-09-2018-11-56-49.jpg" alt="Card image cap" />
                              <div className="card-body">
                                 <div className="">
                                    <div className="card-body-likes">
                                       <i className="fas fa-heart"></i>
                                       <span>83%</span>
                                    </div>
                                    <div className="movie-name">
                                       <span>peta</span>
                                    </div>
                                 </div>
                                 <div className="">
                                    <div className="movie-type">
                                       <span>UA | action | Tamil </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div className="col-md-4">
                           <div className="card">
                              <img className="card-img-top" src="///in.bmscdn.com/iedb/movies/images/mobile/thumbnail/xlarge/maari-2-et00090748-11-12-2018-03-29-15.jpg" alt="Card image cap" />
                              <div className="card-body">
                                 <div className="">
                                    <div className="card-body-likes">
                                       <i className="fas fa-heart"></i>
                                       <span>70%</span>
                                    </div>
                                    <div className="movie-name">
                                       <span>Marri 2</span>
                                    </div>
                                 </div>
                                 <div className="">
                                    <div className="movie-type">
                                       <span>UA | action | Tamil </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div className="col-md-4">
                           <div className="card">
                              <img className="card-img-top" src="//in.bmscdn.com/iedb/movies/images/mobile/thumbnail/xlarge/kgf-et00061448-29-08-2017-10-29-43.jpg" alt="Card image cap" />
                              <div className="card-body">
                                 <div className="">
                                    <div className="card-body-likes">
                                       <i className="fas fa-heart"></i>
                                       <span>87%</span>
                                    </div>
                                    <div className="movie-name">
                                       <span>KGF</span>
                                    </div>
                                 </div>
                                 <div className="">
                                    <div className="movie-type">
                                       <span>UA | action | Tamil </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div className="row movieshead">
                        <div className="col-md-6">
                           <h4>More Movies</h4>
                        </div>
                        <div className="col-md-6">
                           <span className="float-right">viewall</span>
                        </div>
                     </div>
                     <div className="row movies">
                        <div className="col-md-4">
                           <div className="card">
                              <img className="card-img-top" src="//in.bmscdn.com/iedb/movies/images/mobile/thumbnail/xlarge/urihindi-et00062444-22-09-2017-10-18-32.jpg" alt="Card image cap" />
                              <div className="card-body">
                                 <div className="">
                                    <div className="card-body-likes">
                                       <i className="fas fa-heart"></i>
                                       <span>90%</span>
                                    </div>
                                    <div className="movie-name">
                                       <span>URI-The Surgical Strike</span>
                                    </div>
                                 </div>
                                 <div className="">
                                    <div className="movie-type">
                                       <span>UA | action | Hindi </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div className="col-md-4">
                           <div className="card">
                              <img className="card-img-top" src="//in.bmscdn.com/iedb/movies/images/mobile/thumbnail/xlarge/cheat-india-et00069287-16-01-2018-02-19-54.jpg" alt="Card image cap" />
                              <div className="card-body">
                                 <div className="">
                                    <div className="card-body-likes">
                                       <i className="fas fa-heart"></i>
                                       <span>67%</span>
                                    </div>
                                    <div className="movie-name">
                                       <span>Why cheat india</span>
                                    </div>
                                 </div>
                                 <div className="">
                                    <div className="movie-type">
                                       <span>UA | action | Hindi </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div className="col-md-4">
                           <div className="card">
                              <img className="card-img-top" src="//in.bmscdn.com/iedb/movies/images/mobile/thumbnail/xlarge/the-accidental-prime-minister-et00058175-07-06-2017-03-18-15.jpg" alt="Card image cap" />
                              <div className="card-body">
                                 <div className="">
                                    <div className="card-body-likes">
                                       <i className="fas fa-heart"></i>
                                       <span>71%</span>
                                    </div>
                                    <div className="movie-name">
                                       <span>The accidental prime minister</span>
                                    </div>
                                 </div>
                                 <div className="">
                                    <div className="movie-type">
                                       <span>UA | action | Hindi </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>




                     <div className="row movieshead">
                        <div className="col-md-6">
                           <h4>Events</h4>
                        </div>
                        <div className="col-md-6">
                           <span className="float-right">viewall</span>
                        </div>
                     </div>
                     <div className="row movies">
                        <div className="col-md-4">
                           <div className="card">
                              <img className="card-img-top" src="//in.bmscdn.com/Events/moviecard/ET00093038.jpg?201901201417" alt="Card image cap" />
                              <div className="card-body event">
                                 
                                 <div className="row">
                                    <div className="col-md-3">
                                       <div className="date">
                                          <span>15</span> <br />
                                          <span>FEB</span>
                                          
                                       </div>
                                    </div>
                                    <div className="col-md-9">
                                       <div className="movie-description" style ={{fontSize : "9px"}}>
                                          <p style ={{fontSize : "12px"}}> Vh1 Supersonic Arcade Feat. Marshmello - Hyderabad</p>
                                          <p>Gachibowli Stadium - Athletic Ground: Hyderabad</p>
                                          <p>Music Shows</p>
                                          <p>Rs. 1999 onwards</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                           </div>
                        </div>

                        <div className="col-md-4">
                           <div className="card">
                              <img className="card-img-top" src="//in.bmscdn.com/Events/moviecard/ET00090652.jpg?201901201417" alt="Card image cap" />
                              <div className="card-body">

                             
                                <div className="row">
                                <div className="col-md-3">
                                       <div className="date">
                                          <span>23</span> <br />
                                          <span>FEB</span>
                                      
                                       </div>
                                    </div>
                                    <div className="col-md-9">
                                       <div className="movie-description" style ={{fontSize : "9px"}}>
                                          <p style ={{fontSize : "12px"}}>Sunburn Klassique</p>
                                          <p>Vagator: Goa</p>
                                          <p>Music Shows</p>
                                          <p>Rs. 4000 onwards</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div className="col-md-4">
                           <div className="card">
                              <img className="card-img-top" src="//in.bmscdn.com/Events/moviecard/ET00088446.jpg?201901201417" alt="Card image cap" />
                              <div className="card-body">
                              
                              <div className="row">
                              <div className="col-md-3">
                                       <div className="date">
                                          <span>25</span> <br />
                                          <span>JAN</span>
                                       </div>
                                    </div>
                                    <div className="col-md-9">
                                       <div className="movie-description" style ={{fontSize : "9px"}}>
                                          <p style ={{fontSize : "12px"}}>Signature Masterclass 3.0 (Hyderabad)</p>
                                          <p>Novotel and HICC Complex: Hyderabad</p>
                                          <p>Meetups</p>
                                          <p>Rs. 499 onwards</p>
                                       </div>
                                    </div>
                                 </div>

                           </div>
                           </div>
                        </div>
                     </div>




                     <div className="row movieshead">
                        <div className="col-md-6">
                           <h4>Plays</h4>
                        </div>
                        <div className="col-md-6">
                           <span className="float-right">viewall</span>
                        </div>
                     </div>
                     <div className="row movies">
                        <div className="col-md-4">
                           <div className="card">
                              <img className="card-img-top" src="//in.bmscdn.com/Events/moviecard/ET00088679.jpg?201901201417" alt="Card image cap" />
                              <div className="card-body">
                             
                               <div className="row">
                                    <div className="col-md-3">
                                       <div className="date">
                                          <span>26</span> <br />
                                          <span>JAN</span>
                                       </div>
                                    </div>
                                    <div className="col-md-9">
                                       <div className="movie-description" style ={{fontSize : "9px"}}>
                                          <p>Gagan Damama Bajyo</p>
                                          <p>Ravindra Bharathi: Hyderabad</p>
                                          <p>Theatre | Hindi</p>
                                          <p>Rs. 250 onwards</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div className="col-md-4">
                           <div className="card">
                              <img className="card-img-top" src="//in.bmscdn.com/Events/moviecard/ET00093955.jpg?201901201417" alt="Card image cap" />
                              <div className="card-body">
                              {/*
                                 <div className="">
                                    <div className="card-body-likes">
                                       <i className="fas fa-heart"></i>
                                       <span>83%</span>
                                    </div>
                                    <div className="movie-name">
                                       <span>peta</span>
                                    </div>
                                 </div>
                                 <div className="">
                                    <div className="movie-type">
                                       <span>UA | action | Tamil </span>
                                    </div>
                                 </div>
                              */}
                               <div className="row">
                                    <div className="col-md-3">
                                       <div className="date">
                                          <span>26</span> <br />
                                          <span>JAN</span>
                                       </div>
                                    </div>
                                    <div className="col-md-9">
                                       <div className="movie-description" style ={{fontSize : "9px"}}>
                                          <p style ={{fontSize : "12px"}}>Shri Tashi Sau and Gappa</p>
                                          <p>Lamakaan: Hyderabad</p>
                                          <p>Theatre | Marathi</p>
                                          <p>Rs. 200 onwards</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div className="col-md-4">
                           <div className="card">
                              <img className="card-img-top" src="//in.bmscdn.com/Events/moviecard/ET00088633.jpg?201901201417" alt="Card image cap" />
                              <div className="card-body">
                             
                               <div className="row">
                                    <div className="col-md-3">
                                       <div className="date">
                                          <span>26</span> <br />
                                          <span>JAN</span>
                                       </div>
                                    </div>
                                    <div className="col-md-9">
                                       <div className="movie-description" style ={{fontSize : "9px"}}>
                                          <p style ={{fontSize : "12px"}}>Swadesh Deepak's COURT MARTIAL by Samahaara</p>
                                          <p>Phoenix Arena: Hyderabad</p>
                                          <p>Theatre | Hindi </p>
                                          <p>Rs. 150 onwards</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>







                     <div className="row movieshead">
                        <div className="col-md-6">
                           <h4>Sports</h4>
                        </div>
                        <div className="col-md-6">
                           <span className="float-right">viewall</span>
                        </div>
                     </div>
                     <div className="row movies">
                        <div className="col-md-4">
                           <div className="card">
                              <img className="card-img-top" src="//in.bmscdn.com/Events/moviecard/ET00090152.jpg?201901201417" alt="Card image cap" />
                              <div className="card-body">
                              
                               <div className="row">
                                    <div className="col-md-3">
                                       <div className="date">
                                          <span>02</span> <br />
                                          <span>FEB</span>
                                       </div>
                                    </div>
                                    <div className="col-md-9">
                                       <div className="movie-description" style ={{fontSize : "9px"}}>
                                          <p style ={{fontSize : "12px"}}>Pubg Tournament Solo Erangel</p>
                                          <p>AK Gamers (Online Tournament): Hyderabad</p>
                                          <p>Shooting</p>
                                          <p>Rs. 299 onwards</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div className="col-md-4">
                           <div className="card">
                              <img className="card-img-top" src="//in.bmscdn.com/Events/moviecard/ET00093024.jpg?201901201417" alt="Card image cap" />
                              <div className="card-body">
                             
                               <div className="row">
                                    <div className="col-md-3">
                                       <div className="date">
                                          <span>27</span> <br />
                                          <span>JAN</span>
                                       </div>
                                    </div>
                                    <div className="col-md-9">
                                       <div className="movie-description" style ={{fontSize : "9px"}}>
                                          <p style ={{fontSize : "12px"}}>The Great Hyderabad Cyclothon & Carnival- 2019</p>
                                          <p>Gachibowli Stadium - Athletic Ground: Hyderabad</p>
                                          <p>Cycling</p>
                                          <p>Rs. 550 onwards</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div className="col-md-4">
                           <div className="card">
                              <img className="card-img-top" src="//in.bmscdn.com/Events/moviecard/ET00093146.jpg?201901201417" alt="Card image cap" />
                              <div className="card-body">
                              {/*
                                 <div className="">
                                    <div className="card-body-likes">
                                       <i className="fas fa-heart"></i>
                                       <span>83%</span>
                                    </div>
                                    <div className="movie-name">
                                       <span>peta</span>
                                    </div>
                                 </div>
                                 <div className="">
                                    <div className="movie-type">
                                       <span>UA | action | Tamil </span>
                                    </div>
                                 </div>
                              */}
                               <div className="row">
                                    <div className="col-md-3">
                                       <div className="date">
                                          <span>27</span> <br />
                                          <span>JAN</span>
                                       </div>
                                    </div>
                                    <div className="col-md-9">
                                       <div className="movie-description" style ={{fontSize : "9px"}}>
                                          <p>India Virtual Marathon / Cyclothon  Earn  Unique</p>
                                          <p>Your Place and Your Time: Hyderabad</p>
                                          <p>Running</p>
                                          <p>Rs. 399 onwards</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>









































                  </div>
               </div>
            </div>
         </div>
      );
   }
}

export default body;
