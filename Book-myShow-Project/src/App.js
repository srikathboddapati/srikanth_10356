import React, { Component } from 'react';
import './App.css';
import Header from './header.js'
import Body from './body.js'
import Footer from './footer.js'
class App extends Component {
  render() {
    return (
      <div className="App">
         <Header />
         <Body />
         <Footer />

      </div>
    );
  }
}

export default App;
